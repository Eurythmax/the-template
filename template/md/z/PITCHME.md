@title[Split-Screen Templates]

## z<br> jump around

@fa[arrow-down text-black]

@snap[south docslink span-50]
[The Template Docs](https://gitpitch.com/docs/the-template)
@snapend



+++?image=template/img/bg/pink.jpg&position=left&size=50% 100%
@title[Heading + List Body]
@snap[west]
@img[split-screen-img span-80](template/img/gif/z.gif)
@snapend
@snap[east text-bold text-pink]
 - (MAC)   brew install z
 - (LINUX) 
   - Put something like this in your bashrc/zshrc:
   
           . /path/to/z.sh
     
            cd around for a while to build up the db.
     
            PROFIT!!
 - preinstalled for oh-my-zsh
@snapend
