@title[Introduction]

<!--
Tip! Get started with this template as follows:
Step 1. the contents of this PITCHME.md file.
Step 2. Start adding your own custom slide content.
Step 3. Copy slide markdown snippets from template/md directory as needed.
-->

@snap[west text-25 text-bold text-white]
GitPitch<br>*The Template*
@snapend

+++?image=template/img/bg/orange.jpg&position=right&size=50% 100%
@title[Heading + List Body]

@snap[west text-16 text-bold text-italic text-orange span-50]
Topics to be covered today
@snapend

@snap[east text-white]
@ol[split-screen-list text-08](false)
- fish, the friendly interactive shell
- z-shell,  bash's advanced brother
- z, jumper
- fzf, the fuzzy finder
- bat, cat + syntax highlighting
- rg, find string in file
- ncdu, analyse and free disk space
@olend
@snapend


---?include=template/md/z/PITCHME.md

@title[The Template Docs]
